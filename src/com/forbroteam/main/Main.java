/**
 * 
 */
package com.forbroteam.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.forbroteam.util.Util;

/**
 * @author Dmitri Bogatenkov
 * 
 */
public class Main {

	/*****/
	private static final String PROJECT_PATH = "C:\\Users\\bogatdm\\Desktop\\ATF";
	private static final String APP_NAME = "OneApp";
	private static final String APP_PACKAGE_NAME = "com.nokia.app.maps.activities.MainActivity";
	private static final String TEST_APP_NAME = "OneAppTest";
	private static final String TEST_APP_PACKAGE_NAME = "com.fob.solutions.oneapp.test";
//	private static final String APP_NAME = "NotePad";
//	private static final String APP_PACKAGE_NAME = "com.example.android.notepad";
//	private static final String TEST_APP_NAME = "NotePadTest";
//	private static final String TEST_APP_PACKAGE_NAME = "com.jayway.test";
	/*****/

	private static final String ANDROID = PROJECT_PATH
			+ "\\sdk\\tools\\android";
	private static final String ANT = PROJECT_PATH + "\\ant\\bin\\ant";
	private static final String ADB = PROJECT_PATH
			+ "\\sdk\\platform-tools\\adb";
	private static final String JARSIGNER = PROJECT_PATH
			+ "\\ant\\jdk\\bin\\jarsigner";
	private static final String ZIPALIGN = PROJECT_PATH
			+ "\\sdk\\tools\\zipalign";
	private static final String JRE_PATH = PROJECT_PATH
			+ "\\ant\\jdk\\jre\\bin";

	private static final String APP_PATH = PROJECT_PATH + "\\" + TEST_APP_NAME;

	private static final String DEBUG_KEYSTORE = "C:\\Users\\bogatdm\\.android\\debug.keystore";

	private static final String INPUT_PATH = APP_PATH + "\\input";
	private static final String OUTPUT_PATH = APP_PATH + "\\output";

//	private static final String TEMP_DEVICE = "001927e9790f3f";
	private static final String TEMP_DEVICE = "0019c66c42648e";

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {

		File dirBin = new File(APP_PATH + "\\bin");
		File dirGen = new File(APP_PATH + "\\gen");
		if (dirBin.exists()) {
			try {
				Util.delete(dirBin);
				Util.delete(dirGen);
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(0);
			}
		}

		resignApk();
		updateLocalProperties();
		createSignedBuild();
		installApk();
		startTest();
	}

	private static void startTest() throws IOException {

		String command = "call start /b "
				+ ADB
				+ " -s "
				+ TEMP_DEVICE
				+ " shell am instrument -w "
				+ TEST_APP_PACKAGE_NAME
//				+ "/pl.polidea.instrumentation.PolideaInstrumentationTestRunner&";
				+ "/android.test.InstrumentationTestRunner&";
		processBuilderFromAppPath(command);
	}

	private static void installApk() throws IOException {

		getDeviceDescription();
		
		String command = "";
		
		command = "call " + ADB + " -s " + TEMP_DEVICE + " uninstall "
				+ APP_PACKAGE_NAME;
		processBuilderFromAppPath(command);

		command = "call " + ADB + " -s " + TEMP_DEVICE + " uninstall "
				+ TEST_APP_PACKAGE_NAME;
		processBuilderFromAppPath(command);

		command = "call " + ADB + " -s " + TEMP_DEVICE + " install -r " + APP_NAME
				+ "-RESIGNED.apk";
		processBuilderFromAppPath(command);

		command = "call " + ADB + " -s " + TEMP_DEVICE + " install -r "
				+ APP_PATH + File.separator + "bin" + File.separator
				+ TEST_APP_NAME + "-debug.apk";
		processBuilderFromAppPath(command);
	}

	private static void getDeviceDescription() throws IOException {

		String command = "call " + ADB + " -s " + TEMP_DEVICE
				+ " shell cat /system/build.prop|grep -i description";
		processBuilderFromAppPath(command);
	}

	private static void resignApk() throws IOException {
		
		Boolean tempStatus = Util.unZipIt(INPUT_PATH + File.separator
				+ APP_NAME + ".apk", OUTPUT_PATH);
		if (tempStatus) {
			File dirMetaInf = new File(OUTPUT_PATH + "\\META-INF");
			Util.delete(dirMetaInf);

			Util.generateFileList(new File(OUTPUT_PATH));
			tempStatus = Util.zipIt(APP_PATH + File.separator + APP_NAME
					+ "-TEMP.apk");

			if (tempStatus) {
				File dirOutput = new File(OUTPUT_PATH);
				Util.delete(dirOutput);

				signApkWithDebugKey();
				verifyApk();
				zipalignApk();

				File tempApkFile = new File(APP_PATH + File.separator
						+ APP_NAME + "-TEMP.apk");
				Util.delete(tempApkFile);
			}
		}
	}

	private static void zipalignApk() throws IOException {
		String command = ZIPALIGN + " -v 4 " + APP_NAME + "-TEMP.apk "
				+ APP_NAME + "-RESIGNED.apk";
		processBuilderFromAppPath(command);
	}

	private static void verifyApk() throws IOException {

		String command = JARSIGNER + " -verify " + APP_NAME + "-TEMP.apk -verbose";
		processBuilderFromAppPath(command);
	}

	private static void signApkWithDebugKey() throws IOException {

		String command = JARSIGNER + " -verbose -keystore " + DEBUG_KEYSTORE
				+ " -storepass android " + APP_NAME
				+ "-TEMP.apk androiddebugkey";
		System.out.println(command);
		processBuilderFromAppPath(command);
	}

	private static void createSignedBuild() throws IOException {

		String command = "call " + ANT + " debug";
		processBuilderFromAppPath(command);
	}

	private static void updateLocalProperties() throws IOException {

		String command = "call " + ANDROID + " update project -p \"" + APP_PATH
				+ "\" -t android-17";
		processBuilderFromAppPath(command);
	}

	public static void processBuilderFromAppPath(String command)
			throws IOException {

		ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", "cd \""
				+ APP_PATH + "\" && " + command);
		builder.redirectErrorStream(true);
		Process p = builder.start();
		BufferedReader r = new BufferedReader(new InputStreamReader(
				p.getInputStream()));
		String line;
		while (true) {
			line = r.readLine();
			if (line == null) {
				break;
			}
			System.out.println(line);
		}
		r.close();
		p.destroy();
	}
}